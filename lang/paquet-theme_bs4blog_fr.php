<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bsblog
// Langue: fr
// Date: 22-03-2020 13:19:21
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4blog_description' => 'Blog simple, 2 colonnes, responsive, aux couleurs douces.',
	'theme_bs4blog_slogan' => 'Blog simple, 2 colonnes, responsive, aux couleurs douces',
);
?>